import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { 
  StyleSheet} from 'react-native';
import ListNeighbours from './ListNeighbours';
import DetailNeighbour from './DetailNeighbour';
import AddNeighbour from './AddNeighbour';

const Stack = createStackNavigator();

export default class App extends React.Component {

  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName="List neighbours">
          <Stack.Screen name="List neighbours" component={ListNeighbours} />
          <Stack.Screen name="Detail" component={DetailNeighbour} />
          <Stack.Screen name="Add" component={AddNeighbour} />
        </Stack.Navigator>
      </NavigationContainer>
    )
  }
}

const styles = StyleSheet.create({

});